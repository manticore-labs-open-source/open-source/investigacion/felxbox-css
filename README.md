# Flexbox css
## Que es Flex
Flexbox lleva ya tiempo funcionando, pero su sintaxis ha cambiado varias veces. La última (en el momento de escribir este artículo), como veréis en el enlace de W3C, es del 17 de Abril del 2014.

El objetivo de FlexBox es crear un modelo de caja o contenedor optimizado para los distintos dispositivos que usan los usuarios de una web. En CSS 2.1 se establecieron cuatro formas de crear una caja: block, inline, table y position. Ahora se crea un modelo de caja nuevo, la caja flexible o FlexBox con display: flex.

Funciona ajustando los tamaños y la disposición de los elementos que se encuentran dentro de un contenedor o caja, de tal manera que se adapten siempre al espacio disponible. Permite posicionar dichos elementos internos con gran facilidad, de manera independiente al orden en el que aparezcan en el código.

Como veremos FlexBox está pensado para componentes de una web o aplicación, a diferencia de Grid Layout, que espero veamos en otro artículo del blog, que está pensado para el diseño y disposición de la web en su conjunto. No es recomendable usar FlexBox para crear la disposición de toda la página, sino sólo para sus componentes.

### Flexbox online

- Para el uso de **flexbox** para principiante se recomienda usar la herramienta online la cual ayuda a obtener  el código css ya listo.

- herramienta online [**flexyboxes**](http://the-echoplex.net/flexyboxes/)

- ejemplo elemento a la derecha y al centro de la página, el contenido también se encuentra en el centro



```
.flex-container {
    display: -webkit-box;
    display: -moz-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-box-direction: normal;
    -moz-box-direction: normal;
    -webkit-box-orient: horizontal;
    -moz-box-orient: horizontal;
    -webkit-flex-direction: row;
    -ms-flex-direction: row;
    flex-direction: row;
    -webkit-flex-wrap: nowrap;
    -ms-flex-wrap: nowrap;
    flex-wrap: nowrap;
    -webkit-box-pack: end;
    -moz-box-pack: end;
    -webkit-justify-content: flex-end;
    -ms-flex-pack: end;
    justify-content: flex-end;
    -webkit-align-content: center;
    -ms-flex-line-pack: center;
    align-content: center;
    -webkit-box-align: center;
    -moz-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    }

.flex-item{
    -webkit-box-ordinal-group: 1;
    -moz-box-ordinal-group: 1;
    -webkit-order: 0;
    -ms-flex-order: 0;
    order: 0;
    -webkit-box-flex: 0;
    -moz-box-flex: 0;
    -webkit-flex: 0 1 auto;
    -ms-flex: 0 1 auto;
    flex: 0 1 auto;
    -webkit-align-self: center;
    -ms-flex-item-align: center;
    align-self: center;
    }
```
![imagen](/imagenes/Selection_012.png)

<a href="https://twitter.com/kevi_n_24" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @kevi_n_24 </a><br>
